//
//  SearchViewController.swift
//  EtsyExample
//
//  Created by Dmitriy Demchenko on 1/3/17.
//  Copyright © 2017 Dmitriy Demchenko. All rights reserved.
//

import UIKit
import Alamofire

protocol SearchViewControllerOutput: SearchModelInput {}
protocol SearchViewControllerInput: SearchModelOutput {}

final class SearchViewController: UIViewController, BarButtonCustomable {
  
  var model: SearchViewControllerOutput!
  
  @IBOutlet fileprivate weak var pickerView: UIPickerView!
  @IBOutlet fileprivate weak var searchItemTextField: InsetTextField!
  @IBOutlet fileprivate weak var searchItemTextFieldTopConstraint: NSLayoutConstraint!
  @IBOutlet fileprivate weak var searchButton: UIButton!
  
  // MARK: - Life cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    subscribeToKeyboardsNotifications()
    setupViews()
    model.loadCategories()
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    if searchItemTextField.isFirstResponder {
      view.endEditing(true)
    } else {
      super.touchesBegan(touches, with: event)
    }
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }

}

extension SearchViewController: SearchViewControllerInput {
  
  func didStartActivity() {
    DispatchQueue.main.async {
      self.showHud(with: tr(key: .searchHudStartLoadCategoriesText))
    }
  }
  
  func didFinishActivity(hasNewData: Bool) {
    DispatchQueue.main.async {
      self.hideHud(
        successStatus: tr(key: .searchHudEndLoadCategoriesText),
        completion: {
          if hasNewData {
            self.pickerView.reloadAllComponents()
          }
      })
    }
  }
  
  func didFinishActivity(error: Error) {
    DispatchQueue.main.async {
      self.hideHud(errorStatus: error.localizedDescription)
    }
  }
  
  func handleValidationError(_ validationError: String) {
    hideHud(errorStatus: validationError)
  }
  
}

fileprivate extension SearchViewController {
  
  func setupViews() {
    navigationController?.navigationBar.flat = true
    navigationItem.backBarButtonItem = buttonWithoutTitle
    navigationItem.title = tr(key: .searchTitle)
    searchButton.setTitle(tr(key: .searchButtonTitle), for: .normal)
    searchItemTextField.placeholder = tr(key: .searchPlaceholderText)
  }
  
  func subscribeToKeyboardsNotifications() {
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(SearchViewController.keyboardShown(_:)),
      name: NSNotification.Name.UIKeyboardDidShow,
      object: nil
    )
  }
  
  dynamic func keyboardShown(_ notification: Notification) {
    guard let userInfo = notification.userInfo as? [String : Any],
      let value = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue else {
        return
    }
    
    let rawFrame = value.cgRectValue
    let keyboardFrame = view.convert(rawFrame, from: nil)
    
    if searchItemTextField.frame.maxY > keyboardFrame.minY {
      searchItemTextFieldTopConstraint.constant = keyboardFrame.height + 20
      UIView.animate(withDuration: 0.3, animations: {
        self.view.layoutIfNeeded()
      })
    }
  }
  
  @IBAction func searchButtonPressed(_ sender: Any) {
    model.searchItems()
  }
  
}

extension SearchViewController: UIPickerViewDataSource {
  
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return model.numberOfCategories()
  }
  
}

extension SearchViewController: UIPickerViewDelegate {
  
  func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
    guard let category = model.category(at: row) else { return nil }
    
    let attributes = [
      NSFontAttributeName : UIFont.systemFont(ofSize: 20),
      NSForegroundColorAttributeName : UIColor(named: .mainTextColor),
      ]
    return NSAttributedString(string: category.longName, attributes: attributes)
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    model.selectCategory(at: row)
  }
  
}

extension SearchViewController: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    model.enteredText(textField.text)
  }
  
}

extension SearchViewController: UIGestureRecognizerDelegate {
  
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
  }
  
}
