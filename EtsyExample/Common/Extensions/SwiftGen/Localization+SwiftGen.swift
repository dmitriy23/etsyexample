// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import Foundation

// swiftlint:disable file_length
// swiftlint:disable type_body_length
enum L10n {
  /// Search
  case searchTitle
  /// Search
  case searchTabbarTitle
  /// Submit
  case searchButtonTitle
  /// What are you shopping for?
  case searchPlaceholderText
  /// Loading categories...
  case searchHudStartLoadCategoriesText
  /// Categories loaded!
  case searchHudEndLoadCategoriesText
  /// Select a category
  case searchCategoryNameValidationError
  /// Entered Text can't be empty
  case searchKeywordsValidationError
  /// Loading items...
  case feedHudStartLoadItemsText
  /// Items loaded!
  case feedHudEndLoadItemsText
  /// No available item
  case feedHudNoAvailableItemText
  /// There are nobody in category
  case feedPlacheholderNoItemsText
  /// Saved Items
  case itemsTitle
  /// Saved Items
  case itemsTabbarTitle
  /// There are nobody in saved items
  case itemsPlacheholderNoSavedItemsText
  /// Item saved in the data base
  case detailsHudSuccessSaveItemText
  /// Item removed from the data base
  case detailsHudSuccessRemoveItemText
  /// Details
  case detailsTitle
  /// Error
  case alertDefaultErrorTile
  /// Success
  case alertDefaultSuccessTile
  /// Information
  case alertDefaultInfoTile
  /// Save
  case commonButtonsSaveButtonTitle
  /// OK
  case commonButtonsOkButtonTitle
}
// swiftlint:enable type_body_length

extension L10n: CustomStringConvertible {
  var description: String { return self.string }

  var string: String {
    switch self {
      case .searchTitle:
        return L10n.tr(key: "search.title")
      case .searchTabbarTitle:
        return L10n.tr(key: "search.tabbar.title")
      case .searchButtonTitle:
        return L10n.tr(key: "search.button.title")
      case .searchPlaceholderText:
        return L10n.tr(key: "search.placeholder.text")
      case .searchHudStartLoadCategoriesText:
        return L10n.tr(key: "search.hud.start_load_categories.text")
      case .searchHudEndLoadCategoriesText:
        return L10n.tr(key: "search.hud.end_load_categories.text")
      case .searchCategoryNameValidationError:
        return L10n.tr(key: "search.category_name.validation_error")
      case .searchKeywordsValidationError:
        return L10n.tr(key: "search.keywords.validation_error")
      case .feedHudStartLoadItemsText:
        return L10n.tr(key: "feed.hud.start_load_items.text")
      case .feedHudEndLoadItemsText:
        return L10n.tr(key: "feed.hud.end_load_items.text")
      case .feedHudNoAvailableItemText:
        return L10n.tr(key: "feed.hud.no_available_item.text")
      case .feedPlacheholderNoItemsText:
        return L10n.tr(key: "feed.placheholder.no_items.text")
      case .itemsTitle:
        return L10n.tr(key: "items.title")
      case .itemsTabbarTitle:
        return L10n.tr(key: "items.tabbar.title")
      case .itemsPlacheholderNoSavedItemsText:
        return L10n.tr(key: "items.placheholder.no_saved_items.text")
      case .detailsHudSuccessSaveItemText:
        return L10n.tr(key: "details.hud.success_save_item.text")
      case .detailsHudSuccessRemoveItemText:
        return L10n.tr(key: "details.hud.success_remove_item.text")
      case .detailsTitle:
        return L10n.tr(key: "details.title")
      case .alertDefaultErrorTile:
        return L10n.tr(key: "alert.default_error_tile")
      case .alertDefaultSuccessTile:
        return L10n.tr(key: "alert.default_success_tile")
      case .alertDefaultInfoTile:
        return L10n.tr(key: "alert.default_info_tile")
      case .commonButtonsSaveButtonTitle:
        return L10n.tr(key: "common_buttons.save_button.title")
      case .commonButtonsOkButtonTitle:
        return L10n.tr(key: "common_buttons.ok_button.title")
    }
  }

  private static func tr(key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

func tr(key: L10n) -> String {
  return key.string
}
