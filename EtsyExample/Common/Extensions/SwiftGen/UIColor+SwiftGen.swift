// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

#if os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  typealias Color = UIColor
#elseif os(OSX)
  import AppKit.NSColor
  typealias Color = NSColor
#endif

extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}

// swiftlint:disable file_length
// swiftlint:disable type_body_length
enum ColorName {
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#131313"></span>
  /// Alpha: 100% <br/> (0x131313ff)
  case defaultBlackColor
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#1692b3"></span>
  /// Alpha: 100% <br/> (0x1692b3ff)
  case defaultBlueColor
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#adaeaa"></span>
  /// Alpha: 100% <br/> (0xadaeaaff)
  case defaultGrayColor
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#fc6923"></span>
  /// Alpha: 100% <br/> (0xfc6923ff)
  case defaultOrangeColor
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f5f5f1"></span>
  /// Alpha: 100% <br/> (0xf5f5f1ff)
  case mainBackgroundColor
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#545454"></span>
  /// Alpha: 100% <br/> (0x545454ff)
  case mainTextColor
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#7abe4a"></span>
  /// Alpha: 100% <br/> (0x7abe4aff)
  case priceTextColor
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#fcfcfc"></span>
  /// Alpha: 100% <br/> (0xfcfcfcff)
  case tabBarBackgroundColor

  var rgbaValue: UInt32 {
    switch self {
    case .defaultBlackColor: return 0x131313ff
    case .defaultBlueColor: return 0x1692b3ff
    case .defaultGrayColor: return 0xadaeaaff
    case .defaultOrangeColor: return 0xfc6923ff
    case .mainBackgroundColor: return 0xf5f5f1ff
    case .mainTextColor: return 0x545454ff
    case .priceTextColor: return 0x7abe4aff
    case .tabBarBackgroundColor: return 0xfcfcfcff
    }
  }

  var color: Color {
    return Color(named: self)
  }
}
// swiftlint:enable type_body_length

extension Color {
  convenience init(named name: ColorName) {
    self.init(rgbaValue: name.rgbaValue)
  }
}
