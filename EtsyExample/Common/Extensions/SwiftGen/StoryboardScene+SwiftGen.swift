// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import Foundation
import UIKit

protocol StoryboardSceneType {
  static var storyboardName: String { get }
}

extension StoryboardSceneType {
  static func storyboard() -> UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: nil)
  }

  static func initialViewController() -> UIViewController {
    guard let vc = storyboard().instantiateInitialViewController() else {
      fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
    }
    return vc
  }
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
  func viewController() -> UIViewController {
    return Self.storyboard().instantiateViewController(withIdentifier: self.rawValue)
  }
  static func viewController(identifier: Self) -> UIViewController {
    return identifier.viewController()
  }
}

protocol StoryboardSegueType: RawRepresentable { }

extension UIViewController {
  func performSegue<S: StoryboardSegueType>(segue: S, sender: AnyObject? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

// swiftlint:disable file_length
// swiftlint:disable type_body_length

struct StoryboardScene {
  enum Details: String, StoryboardSceneType {
    static let storyboardName = "Details"

    case detailsViewControllerScene = "DetailsViewController"
    static func instantiateDetailsViewController() -> DetailsViewController {
      guard let vc = StoryboardScene.Details.detailsViewControllerScene.viewController() as? DetailsViewController
      else {
        fatalError("ViewController 'DetailsViewController' is not of the expected class DetailsViewController.")
      }
      return vc
    }
  }
  enum Items: String, StoryboardSceneType {
    static let storyboardName = "Items"

    static func initialViewController() -> ItemsViewController {
      guard let vc = storyboard().instantiateInitialViewController() as? ItemsViewController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }

    case itemsViewControllerScene = "ItemsViewController"
    static func instantiateItemsViewController() -> ItemsViewController {
      guard let vc = StoryboardScene.Items.itemsViewControllerScene.viewController() as? ItemsViewController
      else {
        fatalError("ViewController 'ItemsViewController' is not of the expected class ItemsViewController.")
      }
      return vc
    }
  }
  enum LaunchScreen: StoryboardSceneType {
    static let storyboardName = "LaunchScreen"
  }
  enum Search: String, StoryboardSceneType {
    static let storyboardName = "Search"

    case feedViewControllerScene = "FeedViewController"
    static func instantiateFeedViewController() -> FeedViewController {
      guard let vc = StoryboardScene.Search.feedViewControllerScene.viewController() as? FeedViewController
      else {
        fatalError("ViewController 'FeedViewController' is not of the expected class FeedViewController.")
      }
      return vc
    }

    case searchViewControllerScene = "SearchViewController"
    static func instantiateSearchViewController() -> SearchViewController {
      guard let vc = StoryboardScene.Search.searchViewControllerScene.viewController() as? SearchViewController
      else {
        fatalError("ViewController 'SearchViewController' is not of the expected class SearchViewController.")
      }
      return vc
    }
  }
  enum TabBar: String, StoryboardSceneType {
    static let storyboardName = "TabBar"

    case tabBarControllerScene = "TabBarController"
    static func instantiateTabBarController() -> TabBarController {
      guard let vc = StoryboardScene.TabBar.tabBarControllerScene.viewController() as? TabBarController
      else {
        fatalError("ViewController 'TabBarController' is not of the expected class TabBarController.")
      }
      return vc
    }
  }
}

struct StoryboardSegue {
}
